import pandas as pd

# Sample data
data = [
    {'p': 196.4044, 's': 'AAPL', 't': "2023-12-19 21:40:17", 'v': 200.0},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-19 21:40:17", 'v': 0.02372},
    {'p': 42155.55, 's': 'BINANCE:BTCUSDT', 't': "2023-12-19 21:40:17", 'v': 0.00279},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-19 21:40:17", 'v': 0.00035},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-19 13:40:17", 'v': 0.00079},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-19 13:40:17", 'v': 0.00949},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-19 13:40:17", 'v': 0.00949},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-19 13:40:17", 'v': 0.02495},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-12 10:40:17", 'v': 0.02495},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-12 13:40:17", 'v': 0.00949},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-12 13:40:17", 'v': 0.00949},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-12 13:40:17", 'v': 0.02495},
    {'p': 42155.56, 's': 'BINANCE:BTCUSDT', 't': "2023-12-12 10:40:17", 'v': 0.02495},
]

# Create a Pandas DataFrame
df = pd.DataFrame(data)

# Convert the 't' column to a Pandas datetime object
df['t'] = pd.to_datetime(df['t'])

# Extract date and hour from the 't' column
df['date'] = df['t'].dt.date
df['hour'] = df['t'].dt.hour

# Group by both date and hour, then calculate the sum of 'v'
result = df.groupby(['date', 's']).agg(max_price=('p', 'max'),min_price=('p','min')).reset_index()

# Show the result
print(result)
