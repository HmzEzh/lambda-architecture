from __future__ import annotations
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python_operator import PythonOperator
import json
from confluent_kafka import Producer, Consumer, KafkaError
import batch_consumer as hive_consumer
def save_data_to_hive():
    hive_consumer.kafka_consumer_worker()
def batch_0():
    pass
def batch_1():
    pass


with DAG(
    "batch_layer",
    default_args={
        "depends_on_past": False,
        "email": ["airflow@example.com"],
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1,
        "retry_delay": timedelta(minutes=5),

    },
    # [END default_args]
    description="...",
    schedule=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=["batch layer, lambda"],
) as dag:

    t1 = PythonOperator(
        task_id="consume_data",
        python_callable=save_data_to_hive,
    )
    t2 = PythonOperator(
        task_id="batch 0",
        python_callable=batch_0,
    )
    t3 = PythonOperator(
        task_id="batch 1",
        python_callable=batch_1,
    )


    # [END basic_task]



    t1>>[t2,t3]
